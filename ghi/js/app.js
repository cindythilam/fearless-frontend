function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
        <div class="card shadow p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-muted">
                ${startDate}-${endDate}
            </div>
        </div>
    </div>
    `;
  }


function convert(stringDate) {
    let strDate = new Date(stringDate).toLocaleDateString('en-us', {year:"numeric", month:"short", day: "numeric"});
    return strDate;
}


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Response is unsuccessful!")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          console.log(detailResponse);
          if (detailResponse.ok) {
            const details = await detailResponse.json();

            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = convert(details.conference.starts);
            console.log(startDate);
            const endDate = convert(details.conference.ends);
            console.log(endDate);
            const location = details.conference.location.name;
            console.log(location);
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.row-cols-3');
            column.innerHTML += html;

          }
        }

      }
    } catch (e) {
      console.error('Error:', e);
      const errorContainer = document.querySelector('.error-container');
      errorContainer.innerHTML = `
        <div class="alert alert-danger" role="alert">
          An error occurred while loading conferences. Please try again later.
        </div>
      `;
    }
  });
